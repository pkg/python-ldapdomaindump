Source: python-ldapdomaindump
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Emmanuel Arias <eamanu@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3-all,
               python3-dnspython,
               python3-ldap3,
               python3-setuptools
Standards-Version: 4.6.2
Homepage: https://github.com/dirkjanm/ldapdomaindump
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-ldapdomaindump
Vcs-Git: https://salsa.debian.org/python-team/packages/python-ldapdomaindump.git
Testsuite: autopkgtest-pkg-python

Package: python3-ldapdomaindump
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}
Description: Active Directory information dumper via LDAP (Python 3)
 This package contains an Active Directory information dumper via LDAP. In an
 Active Directory domain, a lot of interesting information can be retrieved via
 LDAP by any authenticated user (or machine). This makes LDAP an interesting
 protocol for gathering information in the recon phase of a pentest of an
 internal network. A problem is that data from LDAP often is not available in
 an easy to read format.
 .
 ldapdomaindump is a tool which aims to solve this problem, by collecting and
 parsing information available via LDAP and outputting it in a human readable
 HTML format, as well as machine readable json and csv/tsv/greppable files.
 .
 This package installs the library for Python 3.
