python-ldapdomaindump (0.9.4-2+apertis0) apertis; urgency=medium

  * Sync from debian/trixie.

 -- Apertis CI <devel@lists.apertis.org>  Sun, 09 Mar 2025 12:42:31 +0000

python-ldapdomaindump (0.9.4-2) unstable; urgency=medium

  * Team upload
  * Cherry-pick upstream commit to drop future requirement
    (Closes: #1058953)

 -- Graham Inggs <ginggs@debian.org>  Thu, 04 Jan 2024 20:34:22 +0000

python-ldapdomaindump (0.9.4-1) unstable; urgency=medium

  * d/control: Bump standards version to 4.6.2 (from 4.6.1; no changes
    needed).
  * d/control: Update my contact information. Also update in d/copyright.
  * New upstream release.

 -- Emmanuel Arias <eamanu@debian.org>  Fri, 13 Oct 2023 19:36:03 -0300

python-ldapdomaindump (0.9.3-2+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Fri, 07 Apr 2023 23:11:29 +0000

python-ldapdomaindump (0.9.3-2) unstable; urgency=low

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Update standards version to 4.5.1, no changes needed.

 -- Sandro Tosi <morph@debian.org>  Fri, 03 Jun 2022 14:39:59 -0400

python-ldapdomaindump (0.9.3-1apertis0) apertis; urgency=medium

  * Import from Debian bullseye.
  * Set component to development.

 -- Apertis package maintainers <packagers@lists.apertis.org>  Tue, 20 Apr 2021 10:11:15 +0200

python-ldapdomaindump (0.9.3-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Bump debhelper compat to v13.

 -- Boyuan Yang <byang@debian.org>  Mon, 20 Jul 2020 16:34:53 -0400

python-ldapdomaindump (0.9.1-2) unstable; urgency=medium

  * Team upload.
  * Source-only upload to allow testing migration.
  * Bump Standards-Version to 4.5.0.

 -- Boyuan Yang <byang@debian.org>  Tue, 31 Mar 2020 11:47:58 -0400

python-ldapdomaindump (0.9.1-1) unstable; urgency=medium

  * Initial release (Closes: #941281)
  * Based this work on ldapdomaindump Kali Package.

 -- Emmanuel Arias <emmanuelarias30@gmail.com>  Fri, 27 Sep 2019 16:03:56 -0300
